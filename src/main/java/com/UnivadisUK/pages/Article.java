package com.UnivadisUK.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Article extends BrowserUtility {

	private static final By ARTICLE_HEADER_LOCATOR = By.xpath("//h1");
	private static final By TWITTER_ICON_LOCATOR = By.xpath("//li [@class = 'twitter']");
	private static final By FACEBOOK_ICON_LOCATOR = By.xpath("//li [@class = 'facebook']");
	private static final By LINKEDIN_ICON_LOCATOR = By.xpath("//li [@class = 'linkedin']");
	private static final By RECOMMENDED_ARTICLES_LOCATOR = By.xpath("//li [@class = 'recommended-news__container']");
	private static final By EMAIL_ICON_LOCATOR = By.xpath("//a [@title = 'Email']");
	private static final By EMAIL_TEXT_BOX_LOCATOR = By.xpath("//input [@name = 'emailAddress']");
	private static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//input [@value = 'Send']");
	private static final By EMAIL_SUCCESS_TEXT_LOCATOR = By.xpath("//div [text() = 'Article sent']");
	
	public boolean isArticleHeaderPresent() {
		return isElementVisible(ARTICLE_HEADER_LOCATOR);
	}
	
	public boolean isTwitterIconPresent() {
		return isElementVisible(TWITTER_ICON_LOCATOR);
	}
	
	public boolean isFacebookIconPresent() {
		return isElementVisible(FACEBOOK_ICON_LOCATOR);
	}
	
	public boolean isLinkedinIconPresent() {
		return isElementVisible(LINKEDIN_ICON_LOCATOR);
	}
	
	public boolean isRecommendedArticlesPresent() {
		return isElementVisible(RECOMMENDED_ARTICLES_LOCATOR);
	}
	
	public void sendEmail(String emailID) {
		clickOn(EMAIL_ICON_LOCATOR);
		enterText(EMAIL_TEXT_BOX_LOCATOR, emailID);
		clickOn(SUBMIT_BUTTON_LOCATOR);
	}
	
	public boolean isEmailSentMessageVisible() {
		return isElementVisible(EMAIL_SUCCESS_TEXT_LOCATOR);
	}
}
