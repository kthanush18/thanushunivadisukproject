package com.UnivadisUK.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Home extends BrowserUtility{
	
	private static final By REGISTER_BUTTON_LOCATOR = By.xpath("//a [@title='Sign up free']");
	private static final By LOGIN_LINK_LOCATOR = By.xpath("//a [@title='Login']");
	
	public Register goToRegisterPage() {
		clickOn(REGISTER_BUTTON_LOCATOR);
		return new Register();
	}
	
	public Login goToLoginPage() {
		clickOn(LOGIN_LINK_LOCATOR);
		return new Login();
	}
}
