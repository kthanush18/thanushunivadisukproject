package com.UnivadisUK.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.web.BrowserUtility;

public class Register extends BrowserUtility {
	
	private static final By TITLE_DROPDOWN_LOCATOR = By.xpath("//span [text() = 'Title']/..");
	private static final By FIRSTNAME_TEXTBOX_LOCATOR = By.name("firstName");
	private static final By LASTNAME_TEXTBOX_LOCATOR = By.name("lastName");
	private static final By EMAIL_TEXTBOX_LOCATOR = By.name("emailAddress");
	private static final By PASSWORD_TEXTBOX_LOCATOR = By.name("password");
	private static final By REPASSWORD_TEXTBOX_LOCATOR = By.name("password_check");
	private static final By CREATEACCOUNT_BUTTON_LOCATOR = By.xpath("//button [text()='Create Account']");
	private static final By PROFESSION_DROPDOWN_LOCATOR = By.xpath("//span [text() = 'Profession']/..");
	private static final By MAINSPECIALTY_DROPDOWN_LOCATOR = By.xpath("//span [text() = 'Main specialty']/..");
	private static final By PHONENUMBER_TEXTBOX_LOCATOR = By.name("phoneNumber");
	private static final By CHECKBOX_LOCATOR = By.xpath("//label[@for=\"checkbox-0-0\"]");
	private static final By CHECKBOX_LOCATOR2 = By.xpath("//label[@for=\"checkbox-1-0\"]");
	private static final By CREATEACCOUNT_BUTTON_LOCATOR2 = By.xpath("//input [@value=\"Create an account\"]");
	private static final By SAVE_BUTTON_LOCATOR = By.xpath("//input [@value=\"Save\"]");
	
	public void registerToUnivadis(String title,String fName,String lName,String email,String password,String profession,String mainSpecialty,String phoneNumber) {
		selectTitleDropdownAndText(TITLE_DROPDOWN_LOCATOR, title);
		enterText(FIRSTNAME_TEXTBOX_LOCATOR, fName);
		enterText(LASTNAME_TEXTBOX_LOCATOR, lName);
		enterText(EMAIL_TEXTBOX_LOCATOR, email);
		enterText(PASSWORD_TEXTBOX_LOCATOR, password);
		enterText(REPASSWORD_TEXTBOX_LOCATOR, password);
		clickOn(CREATEACCOUNT_BUTTON_LOCATOR);
		selectDropdownAndText(PROFESSION_DROPDOWN_LOCATOR, profession);
		selectDropdownAndText(MAINSPECIALTY_DROPDOWN_LOCATOR, mainSpecialty);
		enterText(PHONENUMBER_TEXTBOX_LOCATOR, phoneNumber);
		clickOn(CHECKBOX_LOCATOR);
		moveToSpecificElement(CHECKBOX_LOCATOR2);
		clickOn(CHECKBOX_LOCATOR2);
		clickOn(CREATEACCOUNT_BUTTON_LOCATOR2);
		waitAndClick(SAVE_BUTTON_LOCATOR);
	}
	
	public String getUserNameFromFnameAndLname(String fName,String lName) {
		return ""+fName+" "+lName+"";
	}
}
