package com.UnivadisUK.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class DashBoard extends BrowserUtility {
	
	private static final By USERNAME_LOCATOR = By.xpath("//span [@data-test=\"user_name\"]");
	private static final By SEARCH_BAR_LOCATOR = By.id("global-search");
	private static final By RESULTS_LOCATOR = By.xpath("//div [@class = 'results']//li/a");
	private static final By HEADLINES_TITLE_LOCATOR = By.xpath("//span [contains(text(),'Headlines')]/..");
	private static final By LATEST_UPDATES_TITLE_LOCATOR = By.xpath("//span [contains(text(),'Latest Updates')]/..");
	private static final By PROFESSIONAL_TITLE_LOCATOR = By.xpath("//div [@class = 'header__title']");
	private static final By NEWS_LIST_ITEMS_LOCATOR = By.xpath("//ul [@id = 'news-list-content']//li");
	private static final By LOAD_MORE_BUTTON_LOCATOR = By.xpath("//a [@title = 'Load more']");
	private static final By PRELOADER_IMAGE_LOCATOR = By.xpath("//span [@class = 'loading_spinner']");
	
	public void waitForDashBoardToLoad() {
		waitForWebelementToLoad(USERNAME_LOCATOR);
	}
	
	public String getUserName_UI() {
		return waitAndGetAttributeValue(USERNAME_LOCATOR, "data-fullname");
	}

	public boolean isUserNameDisplayed() {
		waitForWebelementToLoad(USERNAME_LOCATOR);
		return isElementVisible(USERNAME_LOCATOR);
	}
	
	public void searchForArticle(String articleName) {
		enterText(SEARCH_BAR_LOCATOR, articleName);
	}
	
	public String getResult() {
		return waitAndGetAttributeValue(RESULTS_LOCATOR, "href");
	}
	
	public void waitForResultsToLoad() {
		waitForWebelementToLoad(RESULTS_LOCATOR);
	}
	
	public Article clickOnSearchResult() {
		clickOn(RESULTS_LOCATOR);
		return new Article();
	}
	
	public boolean isHeadlinesTitlePresent() {
		return isElementVisible(HEADLINES_TITLE_LOCATOR);
	}
	
	public boolean isLatestUpdatesTitlePresent() {
		return isElementVisible(LATEST_UPDATES_TITLE_LOCATOR);
	}
	
	public boolean isProfessionalAndBusinessTitlePresent() {
		return isElementVisible(PROFESSIONAL_TITLE_LOCATOR);
	}
	
	public int getArticlesCount() {
		return getCountOfWebElements(NEWS_LIST_ITEMS_LOCATOR);
	}
	
	public void loadMoreArticles() {
		waitAndClick(LOAD_MORE_BUTTON_LOCATOR);
		waitForInvisibilityOfElement(PRELOADER_IMAGE_LOCATOR);
	}
}
