package com.UnivadisUK.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Login extends BrowserUtility{
	
	private static final By EMAIL_TEXTBOX_LOCATOR = By.xpath("//input [@name = 'userId']");
	private static final By PASSWORD_TEXTBOX_LOCATOR = By.xpath("//input [@name = 'password']");
	private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//button [@class = 'mdscp-button--submit mdscp-button--medium']");
	//private static final By 
	
	public DashBoard loginWithCredentials(String email,String password) {
		enterText(EMAIL_TEXTBOX_LOCATOR, email);
		enterText(PASSWORD_TEXTBOX_LOCATOR, password);
		clickOn(LOGIN_BUTTON_LOCATOR);
		return new DashBoard();
	}

}
