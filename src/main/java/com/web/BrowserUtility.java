package com.web;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserUtility {
	public static WebDriver _driver;
	
	public void launchLocalBrowser(BrowserType browserType) {
		try {
			switch(browserType) {
			case CHROME:
				WebDriverManager.chromedriver().setup();
				_driver = new ChromeDriver();
				break;
			case FIREFOX:
				WebDriverManager.firefoxdriver().setup();
				_driver = new FirefoxDriver();
				break;
			case IE:
				WebDriverManager.iedriver().setup();
				_driver = new InternetExplorerDriver();
				break;
			case EDGE:
				WebDriverManager.edgedriver().setup();
				_driver = new EdgeDriver();
				break;
			case GRID:
				DesiredCapabilities capsCapabilities = new DesiredCapabilities();
				capsCapabilities.setCapability("browserName", "chrome");
				_driver = new RemoteWebDriver(new java.net.URL(ReadMyProperties.getMyProperty("GRID_URL")),capsCapabilities);
			}
		}
		catch(Exception E) {
			System.out.println("Failed to launch browser!!");
		}
	}
	
	public void navigateToURL(String URL) {
		_driver.get(URL);
		_driver.manage().window().maximize();
	}
	
	public void clickOn(By selector) {
		_driver.findElement(selector).click();
	}
	
	public void enterText(By selector,String text) {
		WebElement element = waitForWebelementToLoad(selector);
		element.sendKeys(text);
	}

	public void selectTitleDropdownAndText(By selector,String text) {
		clickOn(selector);
		clickOn(By.xpath("//input [@value = '"+text+"']/../.."));
	}
	
	public void selectDropdownAndText(By selector,String text) {
		clickOn(selector);
		clickOn(By.xpath("//span [text() = '"+text+"']/../.."));
	}
	
	public String getAttributeValue(By selector,String attributeName) {
		return _driver.findElement(selector).getAttribute(attributeName);
	}
	
	public WebElement getElement(By selector) {
		return _driver.findElement(selector);
	}
	
	public void moveToSpecificElement(By selector) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
	}
	
	public WebElement waitForWebelementToLoad(By selector) {
		WebDriverWait wait = new WebDriverWait(_driver, 20);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
	}
	
	public List<WebElement> waitForWebelementsToLoad(By selector) {
		WebDriverWait wait = new WebDriverWait(_driver, 20);
		return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
	}
	
	public void waitAndClick(By selector) {
		WebElement element = waitForWebelementToLoad(selector);
		element.click();
	}
	
	public String waitAndGetAttributeValue(By selector,String attributeName) {
		return waitForWebelementToLoad(selector).getAttribute(attributeName);
	}
	
	public boolean isElementVisible(By selector) {
		WebElement element = waitForWebelementToLoad(selector);
		return element.isDisplayed();
	}
	
	public String getText(By selector) {
		WebElement element = waitForWebelementToLoad(selector);
		return element.getText();
	}
	
	public int getCountOfWebElements(By selector) {
		List<WebElement> elements = waitForWebelementsToLoad(selector);
		return elements.size();
	}
	
	public void waitForInvisibilityOfElement(By selector) {
		WebDriverWait wait = new WebDriverWait(_driver, 20);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
	}
	
	public void quitBrowser() {
		_driver.close();
		_driver.quit();
	}
}
