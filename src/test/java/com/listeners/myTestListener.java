package com.listeners;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class myTestListener implements ITestListener {

	ExtentHtmlReporter extentHtmlReporter;
	ExtentReports extentReports;
	ExtentTest extentTest;
	
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		extentTest = extentReports.createTest(result.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		extentTest.pass(result.getMethod().getMethodName());
		extentTest.log(Status.INFO, result.getMethod().getDescription());
	}

	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		extentTest.fail(result.getMethod().getMethodName());
		extentTest.log(Status.INFO, result.getMethod().getDescription());
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		extentTest.skip(result.getMethod().getMethodName());
		extentTest.log(Status.INFO, result.getMethod().getDescription());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedWithTimeout(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy-hh-mm-ss");
		String dateFormat = format.format(date);
		
		extentHtmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir")+"//testReports//report"+dateFormat+".html");
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentHtmlReporter);
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		extentReports.flush();
	}
	
}
