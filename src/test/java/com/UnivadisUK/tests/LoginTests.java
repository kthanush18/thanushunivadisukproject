package com.UnivadisUK.tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.UnivadisUK.pages.Home;
import com.web.BrowserType;
import com.web.ReadMyProperties;

@Listeners(com.listeners.myTestListener.class)
public class LoginTests extends TestBase {
	
	@BeforeMethod
	public void testIntialize() {
		_home = new Home();
		_home.launchLocalBrowser(BrowserType.CHROME);
		_home.navigateToURL(ReadMyProperties.getMyProperty("URL"));
		_login = _home.goToLoginPage();
	}
	
	@Test (testName = "login to univadis",description = "login using credentials to univadis",dataProviderClass = com.dataproviders.LoginDataProvider.class,dataProvider = "loginDataProviderCSV")
	public void loginToUnivadis(String email,String password) {
		_dashBoard = _login.loginWithCredentials(email,password);
		
		assertTrue(_dashBoard.isUserNameDisplayed());
	}

	@AfterMethod
	public void testCleanUp() {
		_home.quitBrowser();
	}
}
