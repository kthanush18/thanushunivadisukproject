package com.UnivadisUK.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.UnivadisUK.pages.Home;
import com.web.BrowserType;
import com.web.ReadMyProperties;

public class RegisterTests extends TestBase {
	
	@BeforeMethod
	public void testIntialize() {
		_home = new Home();
		_home.launchLocalBrowser(BrowserType.CHROME);
		_home.navigateToURL(ReadMyProperties.getMyProperty("URL"));
		_register = _home.goToRegisterPage();
	}
	
	@Test (testName = "Register to univadis", description = "fill all the required fields and register!!",dataProviderClass = com.dataproviders.RegisterDataProvider.class,dataProvider = "RegisterDataProviderCSV")
	public void registrationTestMethod(String title,String fName,String lName,String email,String password,String profession,String mainSpecialty,String phoneNumber) {
		
		_register.registerToUnivadis(title,fName,lName,email,password,profession,mainSpecialty,phoneNumber);
		String userNameFromProperties = _register.getUserNameFromFnameAndLname(fName,lName);
		String userName_UI = _dashBoard.getUserName_UI();
		
		assertEquals(userNameFromProperties, userName_UI);
	}
	
	@AfterMethod
	public void testCleanUp() {
		_home.quitBrowser();
	}

}
