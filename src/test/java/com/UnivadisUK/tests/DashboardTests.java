package com.UnivadisUK.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.UnivadisUK.pages.Home;
import com.web.BrowserType;
import com.web.ReadMyProperties;

@Listeners(com.listeners.myTestListener.class)
public class DashboardTests extends TestBase {
	
	@BeforeMethod()
	public void testIntialize() {
		_home = new Home();
		_home.launchLocalBrowser(BrowserType.GRID);
		_home.navigateToURL(ReadMyProperties.getMyProperty("URL"));
		_login = _home.goToLoginPage();
		_dashBoard = _login.loginWithCredentials(ReadMyProperties.getMyProperty("email"),ReadMyProperties.getMyProperty("password"));
		_dashBoard.waitForDashBoardToLoad();
	}
	
	@Test (testName = "Search a news article",description = "Search for a news article and verify result")
	public void SearchForNewsArticle() {
		_dashBoard.searchForArticle(ReadMyProperties.getMyProperty("newsArticleName"));
		
		assertEquals(_dashBoard.getResult(),"https://www.univadis.co.uk/viewarticle"
				+ "/gps-should-consider-dyspnoea-and-cough-as-potential-predictors-of-lung-cancer-711320?s1=news");
	}
	
	@Test (testName = "Verify headings in news home page",description = "Verify the headers present on the page")
	public void CheckHeadingsInNewsHomePage() {
		assertTrue(_dashBoard.isHeadlinesTitlePresent());
		assertTrue(_dashBoard.isLatestUpdatesTitlePresent());
		assertTrue(_dashBoard.isProfessionalAndBusinessTitlePresent());
	}
	
	@Test (testName = "Verify articles from latest updates",description = "count the number of articles in latest updates")
	public void CountArticlesFromLatestUpdates() {
		assertEquals(_dashBoard.getArticlesCount(), 14);
		_dashBoard.loadMoreArticles();
		assertEquals(_dashBoard.getArticlesCount(), 28);
	}

	@AfterMethod
	public void testCleanUp() {
		_home.quitBrowser();
	}
}

