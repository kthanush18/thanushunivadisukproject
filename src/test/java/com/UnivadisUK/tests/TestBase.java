package com.UnivadisUK.tests;

import com.UnivadisUK.pages.Article;
import com.UnivadisUK.pages.DashBoard;
import com.UnivadisUK.pages.Home;
import com.UnivadisUK.pages.Login;
import com.UnivadisUK.pages.Register;

public class TestBase {
	
	protected static Home _home;
	protected static Register _register;
	protected static DashBoard _dashBoard;
	protected static Login _login;
	protected static Article _article;

}
