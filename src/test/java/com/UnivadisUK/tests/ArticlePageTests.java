package com.UnivadisUK.tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.UnivadisUK.pages.Home;
import com.web.BrowserType;
import com.web.ReadMyProperties;

@Listeners(com.listeners.myTestListener.class)
public class ArticlePageTests extends TestBase {
	
	@BeforeClass
	public void TestIntialize() {
		_home = new Home();
		_home.launchLocalBrowser(BrowserType.GRID);
		_home.navigateToURL(ReadMyProperties.getMyProperty("URL"));
		_login = _home.goToLoginPage();
		_dashBoard = _login.loginWithCredentials(ReadMyProperties.getMyProperty("email"), ReadMyProperties.getMyProperty("password"));
		_dashBoard.waitForDashBoardToLoad();
		_dashBoard.searchForArticle(ReadMyProperties.getMyProperty("newsArticleName"));
		_dashBoard.waitForResultsToLoad();
		_article = _dashBoard.clickOnSearchResult();
	}

	@Test(testName = "Verify article header", description = "Verify article header is displayed correctly")
	public void VerifyArticleHeader() {
		assertTrue(_article.isArticleHeaderPresent());
	}
	
	@Test(testName = "Verify twitter icon", description = "Verify twitter icon is displayed correctly")
	public void VerifyTwitterIconPresent() {
		assertTrue(_article.isArticleHeaderPresent());
	}
	
	@Test(testName = "Verify facebook icon", description = "Verify facebook icon is displayed correctly")
	public void VerifyFacebookIconPresent() {
		assertTrue(_article.isArticleHeaderPresent());
	}
	
	@Test(testName = "Verify linkedin icon", description = "Verify linkedin icon is displayed correctly")
	public void VerifyLinkedinIconPresent() {
		assertTrue(_article.isArticleHeaderPresent());
	}
	
	@Test(testName = "Verify recommended articles", description = "Verify recommended articles is displayed correctly")
	public void VerifyRecommendedArticles() {
		assertTrue(_article.isArticleHeaderPresent());
	}
	
	@Test(testName = "Verify send email feature", description = "Send email and check email sent succesfull!!!")
	public void VerifySendEmailFeature() {
		_article.sendEmail(ReadMyProperties.getMyProperty("email"));
		
		assertTrue(_article.isEmailSentMessageVisible());
	}
	
	@AfterClass
	public void TestCleanUp() {
		_home.quitBrowser();
	}
}
