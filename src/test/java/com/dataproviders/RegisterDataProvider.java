package com.dataproviders;

import java.util.Iterator;

import org.testng.annotations.DataProvider;

import com.web.ReadFromFiles;

public class RegisterDataProvider {
	
	@DataProvider(name = "RegisterDataProviderCSV")
	public Iterator<String[]> RegisterData()  {
		Iterator<String[]> result = ReadFromFiles.readDataFromCSVFile("registerData.csv");
		return result;
	}

}
